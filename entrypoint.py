#!/usr/bin/env python
import sys

from providers.helper import *
from providers.SwiftProvider import *
from providers.AwsProvider import *
from providers.MinioProvider import *


AWS = 'aws'.lower()
SWIFT = 'swift'.lower()
MINIO = 'minio'.lower()


def get_storage_client(storage_provider):
    """
    Return the python client for the configured env parameter.
    storage_provider: aws / swift / minio
    return: client class object for interaction with S3
    """

    if storage_provider.lower() == AWS:
        return boto3.client('s3')
    elif storage_provider.lower() == SWIFT:
        return swift_client()
    elif storage_provider.lower() == MINIO:
        return minio_client()

    raise ValueError(f"Unsupported storage provider: {storage_provider}")




def upload_to_bucket(client, file_path, bucket_name, img_name, version):
    '''
    dispatcher to right function for upload
    client: client object for connection to backend
    file_path: file to upload
    version: version of the release
    bucket_name: bucket to upload to
    img_name: name of the qcow
    '''
    if isinstance(client, Minio):
        upload_to_minio(client, file_path, bucket_name, img_name, version)
    elif isinstance(client, BaseClient):
        upload_to_s3(client, file_path, bucket_name, img_name, version)
    else:
        raise NotImplementedError(f"Uploading to this provider is not supported. Mission upload implementiation")

def test_connection(client, bucket_name):
    '''
    client: client class object to try connection on
    bucket_name: name of bucket to connect to
    '''
    if isinstance(client, BaseClient):
        try:
            client.list_buckets()
        except Exception as e:
            raise ValueError(f"Error connecting to AWS S3: {e}")
    elif isinstance(client, swiftclient.client.Connection):
        try:
            client.head_account()
        except swiftclient.ClientException as e:
            raise ValueError(f"Error connecting to Swift: {e}")
    elif isinstance(client, Minio):
        try:
            client.bucket_exists(bucket_name)
        except S3Error as e:
            raise ValueError(f"Error connecting to MinIO: {e}")
    else:
        raise ValueError("Unsupported client.")

    return True



if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise ValueError("python entrypoint.py <input_file>")
    input_file = sys.argv[1]
    
    # Get environment variables
    img_name = get_env_variable('IMG_NAME')
    version = get_env_variable('CI_COMMIT_TAG')
    bucket_name = get_env_variable('S3_BUCKET')
    storage_provider = get_env_variable('STORAGE_PROVIDER')

    # Check connection to storage provider
    client = get_storage_client(storage_provider.lower())
    test_connection(client, bucket_name)
    
    # Rename and move the input file to be ready to be uploaded with its MD5, SHA-1, and SHA-256 hashes
    renamed_file = prepare_file_to_upload(input_file, img_name, version)
    calculate_hashes(renamed_file,img_name,version)
    
    # Upload file and hashes to appropriate bucket
    upload_to_bucket(client, renamed_file, bucket_name, img_name, version)
    client.close()
