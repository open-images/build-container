from providers import helper

import swiftclient

def swift_client():
    auth_url = get_env_variable('SWIFT_AUTH_URL')
    project_name = get_env_variable('SWIFT_PROJECT_NAME')
    username = get_env_variable('SWIFT_USERNAME')
    password = get_env_variable('SWIFT_PASSWORD')
    
    conn = swiftclient.Connection(
        authurl=auth_url,
        user=username,
        key=password,
        tenant_name=project_name,
        auth_version='3'
    )
    return conn
