import hashlib
import os
import shutil

def get_env_variable(var_name):
    '''
    helper function to get envitonment variables
    var_name: list of variables to get from env
    return: value of the variable or raise exception if not def 
    '''
    value = os.getenv(var_name)
    if not value:
        raise ValueError(f"The environment variable : {var_name} is not defined")
    return value

def prepare_file_to_upload(input_file_path, img_name, version):
    '''
    input_file_path: path of the file to be prepared
    img_name: name of the image
    return: name of the file to be uploaded
    '''
    input_file = os.path.join(input_file_path)
    output_file = os.path.join(f"{img_name}-{version}.qcow2")

    try:
        shutil.copy(input_file, output_file)
        print(f"The file '{input_file}' has been renamed to '{output_file}'.")
    except FileNotFoundError:
        raise ValueError(f"The file '{input_file}' does not exist.")

    return output_file

def calculate_hashes(file_path,img_name,version):
    '''
    Function to calculate MD5, SHA-1, and SHA-256 hashes of a file
    file_path: path of the file
    '''
    md5_hash = hashlib.md5()
    sha1_hash = hashlib.sha1()
    sha256_hash = hashlib.sha256()

    with open(file_path, "rb") as f:
        # Read in chunks for large files
        while chunk := f.read(4096):
            md5_hash.update(chunk)
            sha1_hash.update(chunk)
            sha256_hash.update(chunk)

    # Get hexadecimal hashes
    md5_digest = md5_hash.hexdigest()
    sha1_digest = sha1_hash.hexdigest()
    sha256_digest = sha256_hash.hexdigest()

    md5_file = os.path.join(f"{img_name}-{version}-md5.txt")
    sha1_file = os.path.join(f"{img_name}-{version}-sha1.txt")
    sha256_file = os.path.join(f"{img_name}-{version}-sha256.txt")

    with open(md5_file, 'w') as f:
        f.write(md5_digest)
    with open(sha1_file, 'w') as f:
        f.write(sha1_digest)
    with open(sha256_file, 'w') as f:
        f.write(sha256_digest)

    print(f"The hashes have been written to files: {md5_file}, {sha1_file}, {sha256_file}")