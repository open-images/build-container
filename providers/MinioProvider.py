from providers import helper

from minio import Minio
from minio.error import S3Error

def minio_client():
    version_folder = f"{version}/"
    latest_folder = "latest/"
    object_name = f"{img_name}-{version}.qcow2"
    latest_object_name = f"{img_name}-latest.qcow2"
    
    minio_endpoint = get_env_variable('MINIO_ENDPOINT')
    minio_access_key = get_env_variable('MINIO_ACCESS_KEY')
    minio_secret_key = get_env_variable('MINIO_SECRET_KEY')
    
    client = Minio(
        minio_endpoint,
        access_key=minio_access_key,
        secret_key=minio_secret_key,
        secure=True
    )
    return client

def upload_to_minio(client, file_path, bucket_name, img_name, version):
    try:
        # Check if version folder exists in the bucket
        if client.bucket_exists(bucket_name):
            objects = client.list_objects(bucket_name, prefix=version_folder, recursive=False)
            version_folder_exists = any(obj.object_name.startswith(version_folder) for obj in objects)
        else:
            raise ValueError(f"Bucket '{bucket_name}' does not exist or is inaccessible.")
        
        # Upload hashes
        md5_file = os.path.join(f"{img_name}-{version}-md5.txt")
        sha1_file = os.path.join(f"{img_name}-{version}-sha1.txt")
        sha256_file = os.path.join(f"{img_name}-{version}-sha256.txt")
        
        client.fput_object(bucket_name, version_folder + f"{object_name}.md5", md5_file)
        client.fput_object(bucket_name, version_folder + f"{object_name}.sha1", sha1_file)
        client.fput_object(bucket_name, version_folder + f"{object_name}.sha256", sha256_file)
        print("Hash files have been uploaded successfully.")
        
        client.fput_object(bucket_name, version_folder + object_name, file_path, part_size=10 * 1024 * 1024)
        print(f"The file '{object_name}' has been uploaded to '{version_folder}' folder in bucket '{bucket_name}'.")
        
        # Upload renamed file to version folder if it exists, otherwise create and upload to both
        if not version_folder_exists:
            client.fput_object(bucket_name, latest_folder + latest_object_name, file_path, part_size=10 * 1024 * 1024)
            client.fput_object(bucket_name, latest_folder + f"{latest_object_name}.md5", md5_file)
            client.fput_object(bucket_name, latest_folder + f"{latest_object_name}.sha1", sha1_file)
            client.fput_object(bucket_name, latest_folder + f"{latest_object_name}.sha256", sha256_file)
            print(
                f"The file '{latest_object_name}' has been uploaded to '{latest_folder}' folder in bucket '{bucket_name}'.")
    except Exception as e:
        raise ValueError(f"Error uploading: {e}")
