from providers.helper import *

import boto3
from botocore.client import BaseClient

def upload_to_s3(client, file_path, bucket_name, img_name, version):
    version_folder = f"{version}/"
    latest_folder = "latest/"
    object_name = f"{img_name}-{version}.qcow2"
    latest_object_name = f"{img_name}-latest.qcow2"
    # Check if version folder exists in the bucket
    try:
        client.head_bucket(Bucket=bucket_name)
        objects = client.list_objects_v2(Bucket=bucket_name, Prefix=version_folder, MaxKeys=1)
        version_folder_exists = 'Contents' in objects
    except Exception as e:
        raise ValueError(f"Bucket '{bucket_name}' does not exist or is inaccessible: {e}")
    try:
        # Upload hashes
        md5_file = os.path.join(f"{img_name}-{version}-md5.txt")
        sha1_file = os.path.join(f"{img_name}-{version}-sha1.txt")
        sha256_file = os.path.join(f"{img_name}-{version}-sha256.txt")
        
        client.upload_file(md5_file, bucket_name, version_folder + f"{object_name}.md5")
        client.upload_file(sha1_file, bucket_name, version_folder + f"{object_name}.sha1")
        client.upload_file(sha256_file, bucket_name, version_folder + f"{object_name}.sha256")
        print("Hash files have been uploaded successfully.")
        
        client.upload_file(file_path, bucket_name, version_folder + object_name)
        print(f"The file '{object_name}' has been uploaded to '{version_folder}' folder in bucket '{bucket_name}'.")
        
        # Upload renamed file to version folder if it exists, otherwise create and upload to both
        if not version_folder_exists:
            client.upload_file(file_path, bucket_name, latest_folder + latest_object_name)
            client.upload_file(md5_file, bucket_name, latest_folder + f"{latest_object_name}.md5")
            client.upload_file(sha1_file, bucket_name, latest_folder + f"{latest_object_name}.sha1")
            client.upload_file(sha256_file, bucket_name, latest_folder + f"{latest_object_name}.sha256")
        
            print(f"The file '{latest_object_name}' has been uploaded to '{latest_folder}' folder in bucket '{bucket_name}'.")
    except Exception as e:
        raise ValueError(f"Error uploading: {e}")
