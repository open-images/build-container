#!/usr/bin/env sh

set -e

[ ! -d "/input" ] && echo "input directory does not exist" && exit 1
[ ! -d "/output" ] && echo "output directory does not exist" && exit 1

echo "Copying input to working directory"
cp -r /input/. /work

echo "Initializing packer"
packer init .

# NOTE(vinetos): Output directory has to not exist because of QEMU Packer builder
rm -r output

echo "Building packer"
packer build .

echo "Copying result to output directory"
cp -r output /output

exit 0
